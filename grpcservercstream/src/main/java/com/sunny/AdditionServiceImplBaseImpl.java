package com.sunny;

import io.grpc.examples.addition.AdditionServiceGrpc;
import io.grpc.examples.addition.Result;
import io.grpc.examples.addition.Value;
import io.grpc.stub.StreamObserver;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author zsunny
 * @Date 2018/6/14 16:28
 * @Mail zsunny@yeah.net
 */
public class AdditionServiceImplBaseImpl extends AdditionServiceGrpc.AdditionServiceImplBase {

    private Logger logger = Logger.getLogger(AdditionServiceImplBaseImpl.class.getName());

    @Override
    public StreamObserver<Value> getResult(final StreamObserver<Result> responseObserver) {
        //如果处理过程较为复杂，可以考虑单独写一个类来实现。这里用匿名内部类的方式，在内部类中用到参数需要加final修饰符
        return new StreamObserver<Value>() {

            private int sum = 0;
            private int cnt = 0;
            private double avg;

            public void onNext(Value value) {
                sum += value.getValue();
                cnt++;
            }

            public void onError(Throwable throwable) {
                logger.log(Level.SEVERE,throwable.getMessage());
            }

            public void onCompleted() {
                avg = 1.0*sum/cnt;
                responseObserver.onNext(Result.newBuilder().setSum(sum).setCnt(cnt).setAvg(avg).build());
                responseObserver.onCompleted();
            }
        };
    }
}
