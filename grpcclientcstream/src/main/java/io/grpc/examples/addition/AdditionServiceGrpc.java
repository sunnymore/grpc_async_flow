package io.grpc.examples.addition;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 * <pre>
 * 定义服务
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.12.0)",
    comments = "Source: additionService.proto")
public final class AdditionServiceGrpc {

  private AdditionServiceGrpc() {}

  public static final String SERVICE_NAME = "addition.AdditionService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @Deprecated // Use {@link #getGetResultMethod()} instead.
  public static final io.grpc.MethodDescriptor<io.grpc.examples.addition.Value,
      io.grpc.examples.addition.Result> METHOD_GET_RESULT = getGetResultMethodHelper();

  private static volatile io.grpc.MethodDescriptor<io.grpc.examples.addition.Value,
      io.grpc.examples.addition.Result> getGetResultMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<io.grpc.examples.addition.Value,
      io.grpc.examples.addition.Result> getGetResultMethod() {
    return getGetResultMethodHelper();
  }

  private static io.grpc.MethodDescriptor<io.grpc.examples.addition.Value,
      io.grpc.examples.addition.Result> getGetResultMethodHelper() {
    io.grpc.MethodDescriptor<io.grpc.examples.addition.Value, io.grpc.examples.addition.Result> getGetResultMethod;
    if ((getGetResultMethod = AdditionServiceGrpc.getGetResultMethod) == null) {
      synchronized (AdditionServiceGrpc.class) {
        if ((getGetResultMethod = AdditionServiceGrpc.getGetResultMethod) == null) {
          AdditionServiceGrpc.getGetResultMethod = getGetResultMethod =
              io.grpc.MethodDescriptor.<io.grpc.examples.addition.Value, io.grpc.examples.addition.Result>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "addition.AdditionService", "getResult"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.grpc.examples.addition.Value.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.grpc.examples.addition.Result.getDefaultInstance()))
                  .setSchemaDescriptor(new AdditionServiceMethodDescriptorSupplier("getResult"))
                  .build();
          }
        }
     }
     return getGetResultMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AdditionServiceStub newStub(io.grpc.Channel channel) {
    return new AdditionServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AdditionServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new AdditionServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AdditionServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new AdditionServiceFutureStub(channel);
  }

  /**
   * <pre>
   * 定义服务
   * </pre>
   */
  public static abstract class AdditionServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * 服务中的方法，用于根据Name类型的参数获得一个Ip类型的返回值
     * </pre>
     */
    public io.grpc.stub.StreamObserver<io.grpc.examples.addition.Value> getResult(
        io.grpc.stub.StreamObserver<io.grpc.examples.addition.Result> responseObserver) {
      return asyncUnimplementedStreamingCall(getGetResultMethodHelper(), responseObserver);
    }

    @Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetResultMethodHelper(),
            asyncClientStreamingCall(
              new MethodHandlers<
                io.grpc.examples.addition.Value,
                io.grpc.examples.addition.Result>(
                  this, METHODID_GET_RESULT)))
          .build();
    }
  }

  /**
   * <pre>
   * 定义服务
   * </pre>
   */
  public static final class AdditionServiceStub extends io.grpc.stub.AbstractStub<AdditionServiceStub> {
    private AdditionServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AdditionServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected AdditionServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AdditionServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * 服务中的方法，用于根据Name类型的参数获得一个Ip类型的返回值
     * </pre>
     */
    public io.grpc.stub.StreamObserver<io.grpc.examples.addition.Value> getResult(
        io.grpc.stub.StreamObserver<io.grpc.examples.addition.Result> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getGetResultMethodHelper(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * 定义服务
   * </pre>
   */
  public static final class AdditionServiceBlockingStub extends io.grpc.stub.AbstractStub<AdditionServiceBlockingStub> {
    private AdditionServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AdditionServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected AdditionServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AdditionServiceBlockingStub(channel, callOptions);
    }
  }

  /**
   * <pre>
   * 定义服务
   * </pre>
   */
  public static final class AdditionServiceFutureStub extends io.grpc.stub.AbstractStub<AdditionServiceFutureStub> {
    private AdditionServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AdditionServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected AdditionServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AdditionServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_GET_RESULT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AdditionServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AdditionServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @Override
    @SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_RESULT:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.getResult(
              (io.grpc.stub.StreamObserver<io.grpc.examples.addition.Result>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AdditionServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AdditionServiceBaseDescriptorSupplier() {}

    @Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return io.grpc.examples.addition.AddtitionProto.getDescriptor();
    }

    @Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AdditionService");
    }
  }

  private static final class AdditionServiceFileDescriptorSupplier
      extends AdditionServiceBaseDescriptorSupplier {
    AdditionServiceFileDescriptorSupplier() {}
  }

  private static final class AdditionServiceMethodDescriptorSupplier
      extends AdditionServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AdditionServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AdditionServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AdditionServiceFileDescriptorSupplier())
              .addMethod(getGetResultMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
